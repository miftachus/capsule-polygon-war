﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using Photon.Realtime;
using Hashtable = ExitGames.Client.Photon.Hashtable;
public class PlayerController : MonoBehaviourPunCallbacks, IDamageable
{
    [SerializeField] Image healthBar;
    [SerializeField] GameObject ui;
    [SerializeField] GameObject camHolder;
    [SerializeField] float mouseSens, sprintSpeed, walkSpeed, JumpForce, SmoothTime;
    [SerializeField] item[] items;

    int itemIndex;
    int prevItemIndex = -1;
    int ammo = 15, ammo2 = 50;

    public Text ammotxt;

    float verticalRotation;
    [SerializeField]
    bool grounded;
    Vector3 smoothMoveVelocity;
    Vector3 moveAmount;

    Rigidbody rb;
    PhotonView PV;

    const float maxHealth = 100f;
    float curHealth = maxHealth;

    PlayerManager playerManager;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        PV = GetComponent<PhotonView>();

        playerManager = PhotonView.Find((int)PV.InstantiationData[0]).GetComponent <PlayerManager>();

    }
    private void Start()
    {
        ammotxt = GameObject.Find("AmmoText").GetComponent<Text>();

        if(PV.IsMine)
        {
            EquipItem(0);
        }
        else
        {
            Destroy(GetComponentInChildren<Camera>().gameObject);
            Destroy(rb);
            Destroy(ui);
        }
    }
    private void Update()
    {
        if (!PV.IsMine)
            return;
        Look();
        Move();
        Jump();

        for(int i = 0; i< items.Length; i++)
        {
            if(Input.GetKeyDown((i+1).ToString()))
            {
                EquipItem(i);
                break;
            }
        }

        if(Input.GetAxisRaw("Mouse ScrollWheel") > 0f)
        {
            if(itemIndex >= items.Length - 1)
            {
                EquipItem(0);
            }
            else
            {
                EquipItem(itemIndex + 1);
            }
        }
        else if (Input.GetAxisRaw("Mouse ScrollWheel") < 0f)
        {
            if (itemIndex <= 0)
            {
                EquipItem(itemIndex + 1);
            }
            else
            {
                EquipItem(itemIndex - 1);
            }
        }
        if (Input.GetMouseButtonDown(0))
        {
            items[itemIndex].Use();
            if (itemIndex <= 0)
            {
                ammo--;
                if(ammo <= 0)
                {
                    EquipItem(itemIndex + 1);
                    ammo = 0;
                }
                ammotxt.text = "Peluru : " + ammo;
            }
            else
            {
                ammo2--;
                if(ammo2 <= 0)
                {
                    EquipItem(itemIndex - 1);
                    ammo2 = 0; 
                }
                ammotxt.text = "Peluru : " + ammo2;
            }
        }

        if(transform.position.y < -10f)
        {
            Die();
        }
    }
    private void FixedUpdate()
    {
        if (!PV.IsMine)
            return;
        rb.MovePosition(rb.position + transform.TransformDirection(moveAmount) * Time.fixedDeltaTime);
    }
    void Jump()
    {
        if (Input.GetKeyDown(KeyCode.Space) && grounded)
        {
            rb.AddForce(transform.up * JumpForce);
        }

    }
    void Move()
    {

        Vector3 moveDir = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized;

        moveAmount = Vector3.SmoothDamp(moveAmount, moveDir * (Input.GetKeyDown(KeyCode.LeftShift) ? sprintSpeed : walkSpeed), ref smoothMoveVelocity, SmoothTime);

    }

    void EquipItem(int _index)
    {
        if (_index == prevItemIndex)
            return;

        itemIndex = _index;
        items[itemIndex].itemGameObject.SetActive(true);

        if(prevItemIndex != -1)
        {
            items[prevItemIndex].itemGameObject.SetActive(false);
        }

        prevItemIndex = itemIndex;

        if(PV.IsMine)
        {
            Hashtable hash = new Hashtable();
            hash.Add("itemIndex", itemIndex);
            PhotonNetwork.LocalPlayer.SetCustomProperties(hash);
        }
    }

    public override void OnPlayerPropertiesUpdate(Player targetPlayer, ExitGames.Client.Photon.Hashtable changedProps)
    {
        if(!PV.IsMine && targetPlayer == PV.Owner)
        {
            EquipItem((int)changedProps["itemIndex"]);
        }
    }

    void Look()
    {
        transform.Rotate(Vector3.up * Input.GetAxisRaw("Mouse X") * mouseSens);

        verticalRotation += Input.GetAxisRaw("Mouse Y") * mouseSens;
        verticalRotation = Mathf.Clamp(verticalRotation, -90f, 90f);

        camHolder.transform.localEulerAngles = Vector3.left * verticalRotation;
    }

    public void SetGroundedBool(bool _grounded)
    {
        if (!PV.IsMine)
            return;
        grounded = _grounded;
    }

    public void TakeDamage(float damage)
    {
        PV.RPC("RPC_TakeDamage", RpcTarget.All, damage);
    }

    [PunRPC]
    void RPC_TakeDamage(float damage)
    {
        if (!PV.IsMine)
            return;
        curHealth -= damage;
        healthBar.fillAmount = curHealth / maxHealth;

        if(curHealth <= 0)
        {

            for (int i = 0; i < PhotonNetwork.PlayerList.Length; i++)
            {
                if (Launcher.instance.myUname == PhotonNetwork.PlayerList[i].NickName)
                {
                    Debug.Log($"{Launcher.instance.myUname} is dead");
                }
            }

            Debug.Log($"Total Player Now: {PhotonNetwork.PlayerList.Length}");

            Die();
        
        }
    }

    void Die()
    {
       playerManager.Die();
    }
}
