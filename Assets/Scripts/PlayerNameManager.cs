﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;
public class PlayerNameManager : MonoBehaviour
{
    [SerializeField] TMP_InputField unInput;

    private void Start()
    {
        if(PlayerPrefs.HasKey("username"))
        {
            unInput.text = PlayerPrefs.GetString("username");
            PhotonNetwork.NickName = PlayerPrefs.GetString("username");
        }
        else
        {
            unInput.text = "Player " + Random.Range(0, 10000).ToString("0000");
            OnUNInputValueChanged();
        }
    }
    public void OnUNInputValueChanged()
    {
        PhotonNetwork.NickName = unInput.text;
        PlayerPrefs.SetString("username", unInput.text);
    }
}
