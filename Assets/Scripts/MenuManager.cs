﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;
using Photon.Realtime;

public class MenuManager : MonoBehaviour
{
    public static MenuManager instance;
    [SerializeField] Menu[] menus;

    private void Awake()
    {
        instance = this;
    }
    public void OpenMenu(string menuName)
    {
        for(int i = 0; i< menus.Length; i++)
        {
            if(menus[i].menuName == menuName)
            {
                menus[i].Open();
            }
            else if(menus[i].open)
            {
                CloseMenu(menus[i]);
            }
        }

    }

    public void OpenMenu(Menu menu)
    {
        menu.Open();
    }
    public void CloseMenu(Menu menu)
    {
        menu.Close();
    }
}
