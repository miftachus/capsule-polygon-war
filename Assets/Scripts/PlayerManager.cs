﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;
    
public class PlayerManager : MonoBehaviour
{
    PhotonView PV;

    GameObject controller;
    void Awake()
    {
        PV = GetComponent<PhotonView>();   
    }

    private void Start()
    {
        if(PV.IsMine)
        {
            CreateController();
        }
    }

    void CreateController()
    {
        Transform spawnPoint = SpawnManager.instance.getSpawnPoint();
        controller = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PlayerController"), spawnPoint.position, spawnPoint.rotation,0, new object[] { PV.ViewID });
        Cursor.visible = false;
    }

    public void Die()
    {
        //PhotonNetwork.Destroy(controller);
        //CreateController();
        Cursor.visible = true;
        PhotonNetwork.LeaveRoom();
    }
    
}
