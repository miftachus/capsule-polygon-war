﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerListUI : MonoBehaviourPunCallbacks
{
    [SerializeField] private GameObject entryPrefab;
    [SerializeField] private GameObject entryPanel;

    private Photon.Realtime.Player[] playerList;

    private void Start()
    {
        UpdateEntry();
    }

    private void UpdateEntry()
    {
        ClearEntry();

        playerList = PhotonNetwork.PlayerList;

        //Return in case player list doesn't exist yet
        if (playerList == null) return;

        foreach (var player in playerList)
        {
            var entry = Instantiate(entryPrefab, entryPanel.transform, true);
            var entryText = entry.GetComponent<TMPro.TMP_Text>();
            entryText.text = player.NickName;

            //Make text color green if it is local player
            if (PhotonNetwork.LocalPlayer == player) entryText.color = Color.green;
        }
    }

    private void ClearEntry()
    {
        foreach (Transform obj in entryPanel.transform)
        {
            Destroy(obj.gameObject);
        }
    }

    public override void OnPlayerEnteredRoom(Photon.Realtime.Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        UpdateEntry();
    }

    public override void OnPlayerLeftRoom(Photon.Realtime.Player otherPlayer)
    {
        base.OnPlayerLeftRoom(otherPlayer);
        UpdateEntry();
    }
}
