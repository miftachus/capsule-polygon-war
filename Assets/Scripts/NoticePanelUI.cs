﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoticePanelUI : MonoBehaviour
{
    [SerializeField] private TMPro.TMP_Text noticeText;
    [SerializeField] private GameObject noticePanel;

    private RoomManager roomManager;
    private GameManager gameManager;

    private void OnEnable()
    {
        roomManager = RoomManager.instance;
        gameManager = GameManager.instance;

        roomManager.OnJoinGame += JoinedGame;
        roomManager.OnStartGame += StartGame;
        roomManager.OnLoseCountdown += LoseCountdown;

        gameManager.OnWinCountdown += WinCountdown;
    }

    private void JoinedGame()
    {
        noticePanel.SetActive(true);
        noticeText.text = "Waiting for other players...";
    }

    private void StartGame()
    {
        noticePanel.SetActive(false);
    }

    private void LoseCountdown(int count)
    {
        noticePanel.SetActive(true);
        noticeText.text = $"<size=200%>YOU LOSE!</size>\nReturning to lobby in {count}...";
    }

    private void WinCountdown(int count)
    {
        noticePanel.SetActive(true);
        noticeText.text = $"<size=200%>YOU WIN!</size>\nReturning to lobby in {count}...";
    }
}
