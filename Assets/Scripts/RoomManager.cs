﻿using System.Collections;
using System;
using UnityEngine;
using Photon.Pun;
using UnityEngine.SceneManagement;
using System.IO;
using Photon.Realtime;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class RoomManager : MonoBehaviourPunCallbacks
{
    public static RoomManager instance;

    public Action<int> OnLoseCountdown;
    public Action OnLeavingRoom;
    public Action OnJoinGame;
    public Action OnStartGame;

    public int leaveRoomDuration = 3;

    private void Awake()
    {
        if(instance)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        instance = this;
    }

    public override void OnEnable()
    {
        base.OnEnable();
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    public override void OnDisable()
    {
        base.OnDisable();
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
    {
        if(scene.buildIndex == 1)
        {
            Debug.Log("Entering Game Scene...");
            Hashtable properties = new Hashtable
            {
                {"PlayerHasLoaded", true }
            };
            PhotonNetwork.LocalPlayer.SetCustomProperties(properties, null);
            Debug.Log("Custom Properties Set!");
            OnJoinGame?.Invoke();
        }
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        base.OnDisconnected(cause);
        Debug.Log("Disconnected: " + cause.ToString());
        SceneManager.LoadScene(0);
    }

    public override void OnLeftRoom()
    {
        base.OnLeftRoom();
        if(SceneManager.GetActiveScene().buildIndex == 1)
        {
            StartCoroutine(LeaveRoomCountdown(leaveRoomDuration));
        }
        else
        {
            //Left Room in Lobby
        }
    }

    public override void OnPlayerPropertiesUpdate(Player targetPlayer, ExitGames.Client.Photon.Hashtable changedProps)
    {
        base.OnPlayerPropertiesUpdate(targetPlayer, changedProps);
        if (changedProps.ContainsKey("PlayerHasLoaded") && CheckAllPlayerHasLoaded())
        {
            PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PlayerManager"), Vector3.zero, Quaternion.identity);
            OnStartGame?.Invoke();
        }
    }


    private IEnumerator LeaveRoomCountdown(int durationInSeconds)
    {
        OnLeavingRoom?.Invoke();
        int timer = durationInSeconds;
        do
        {
            OnLoseCountdown?.Invoke(timer);
            yield return new WaitForSecondsRealtime(1f);
            timer--;
        } while (timer > 0);
        SceneManager.LoadScene(0);
        yield return null;
    }

    private bool CheckAllPlayerHasLoaded()
    {
        foreach (Player player in PhotonNetwork.PlayerList)
        {
            object playerLoadedLevel;

            if (player.CustomProperties.TryGetValue("PlayerHasLoaded", out playerLoadedLevel))
            {
                if ((bool)playerLoadedLevel)
                {
                    Debug.Log(player.NickName + " has loaded!");
                    continue;
                }
            }

            Debug.Log(player.NickName + " has not loaded!");

            return false;
        }

        Debug.Log("All player has loaded!");

        return true;
    }
}
