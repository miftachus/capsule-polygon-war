﻿using System.Collections;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;

public class GameManager : MonoBehaviourPunCallbacks
{
    public static GameManager instance;

    public Action OnGameWin;
    public Action<int> OnWinCountdown;
    public int winDelay;

    private RoomManager roomManager;

    private void Awake()
    {
        SingletonInit();

        roomManager = RoomManager.instance;
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        base.OnPlayerLeftRoom(otherPlayer);
        CheckWin();
    }

    void CheckWin()
    {
        var playerList = PhotonNetwork.PlayerList;

        //Check if there's only 1 player left AND that player is our local player
        if(playerList.Length <= 1 && playerList[0] == PhotonNetwork.LocalPlayer)
        {
            StartCoroutine(WinCoroutine());
        }
    }

    IEnumerator WinCoroutine()
    {
        OnGameWin?.Invoke();
        int timer = winDelay;
        do
        {
            OnWinCountdown?.Invoke(timer);
            yield return new WaitForSecondsRealtime(1f);
            timer--;
        } while (timer > 0);
        SceneManager.LoadScene(0);
    }

    void SingletonInit()
    {
        if (instance)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }


}
