# CAPSULE POLYGON WAR 

## Development Team  ##

4210181014 M Miftachus Tsaqif 


## DESCRIPTION 
--------------------------

This game is a 3d game made using unity and added a photon framework in it, this game is a simple fps game that uses a mouse and keyboard with a simple game mechanism. This game has a battle royale theme where players compete with each other to survive the longest from other players' shots


## Genre
------------------------------------

Battle Royale 
MultiPlayer
3D


## Project Repository 
--------------------------------------------
Project [https://gitlab.com/miftachus/capsule-polygon-war]

## Installation 
----------------------------------------------
. Download at [https://drive.google.com/file/d/1yiETf-cF8TBPZl7ERsU6QP6vUxNuMY9q/view?usp=sharing]
. Extract The Game and Play the .exe file 


# Project Development 
________________________________

## Game Engine 

This game is Unity 2019.4.12 Lts Version 

![alt-text](https://upload.wikimedia.org/wikipedia/commons/8/8a/Official_unity_logo.png)<br>


#### <i>Clone</i> Project App

git

```bash
https://gitlab.com/miftachus/capsule-polygon-war
```



## Framework
#### [Photon](https://www.photonengine.com/)

![alt-text](https://www.photonengine.com/Content/img/nav-logo-photon.png)<br>

## Photon Overview 

Photon slogan is 

We Make
Multiplayer Simple

and photon is made for anyone: indies, professional studios and AAA productions. this is a simple plugin with udp based protocol
The Photon Realtime SDK is the lean and core API to access all Photon Cloud Services. 

Photon Realtime is our base layer for multiplayer games and higher-level network solutions. It solves problems like matchmaking and fast communication with a scalable approach. It is used by games and our more specific multiplayer solutions PUN and Quantum.

The term Photon Realtime also wraps up our comprehensive framework of APIs, software tools and services and defines how the clients and servers interact with one another.

The Photon binary protocols are organized in several layers. On its lowest level, UDP is used to carry the messages you want to send. Within those standard datagrams, several headers reflect the properties you expect of Photon: optional reliability, sequencing, aggregation of messages, time synchronization and various others.

The following chart shows the individual layers and their nesting:
![alt-text](https://doc.photonengine.com/docs/img/BinaryProtocol-udp-layers.png)<br>


## Game Flow 

![ALT](https://gitlab.com/miftachus/capsule-polygon-war/-/raw/de5bc99193fded54fbe502f8b6764a83f982a1e5/Assets%20Readme.md/flowgame.png)
-------------------------------------------------
#Code Documentation
-------------------------------------------------
## Join Photon Master Server 

```bash
Assets/Scripts/Launcher.cs
```

## Join  or Create Room 

```bash
Assets/Scripts/Launcher.cs
```

## Room List Update

```bash
Assets/Scripts/RoomListItem.cs
```

## Ammo Shoot Mechanism

```bash
Assets/Scripts/PlayerController.cs
```


## Win condition Check

```bash
Assets/Scripts/GameManager.cs
```


______________________________________________________
______________________________________________________

# Documentation Game 

## Main Menu
![alt-text](Assets Readme.md/main menu.gif)

## Join room and Start Game 
![alt-text](Assets Readme.md/start game.gif)

## Kill Other Player 
![alt-text](Assets Readme.md/kill player.gif)











